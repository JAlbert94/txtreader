import java.io.*;
import java.util.*;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {

		
		ArrayList<String> temp= new ArrayList<String>();
		
		String line;
		try {

			File f = new File("myFile.txt");

			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			
			
			line = br.readLine();
			
			while (line != null) {
				
				temp.add(line);
				line=br.readLine();
			}
			
			Collections.reverse(temp);
			System.out.println(temp);
			
			FileWriter fw= new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i=0;i<temp.size();i++){
				
				bw.write(temp.get(i).toString());
				bw.newLine();
				bw.flush();  
				
			}
			
			fw.close();
			fr.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Archivo no encontrado.",
					"Compruebe el nombre del archivo.",
					JOptionPane.ERROR_MESSAGE);
		}
		
		System.out.println("Ended.");
		
	}

}
