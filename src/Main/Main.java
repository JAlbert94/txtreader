package Main;

import java.io.*;
import java.util.*;

import javax.swing.JOptionPane;

public class Main {
/**
 * lectura de un txt llamado myFile por lineas y inversion de dichas lineas.
 * @param args
 * @temp ArrayList<T> Almacena temportalmente las lineas leidas del txt. 
 * @f Archivo a abrir.
 * @fr Lector del archivo f.
 * @br BufferedReader usado para rellenar la variable temp.
 * @line String Almacena temporalmete la ultima linea leida.
 */
	public static void main(String[] args) {

		ArrayList<String> temp= new ArrayList<String>();
		
		String line;
		
		try {
			/**
			 
			 */
			File f = new File("myFile.txt");

			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			
			/**
			 * Bucle usado para leer y guardar el contenido del archivo.
			 */
			line = br.readLine();
			
			
			while (line != null) {
				
				temp.add(line);
				line=br.readLine();
			}
			/**
			 * Reversion del ArrayList.
			 */
			Collections.reverse(temp);
			System.out.println(temp);
			/**
			 * Reescritura del archivo con sus lineas invertidas.
			 */
			FileWriter fw= new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i=0;i<temp.size();i++){
				
				bw.write(temp.get(i).toString());
				bw.newLine();
				bw.flush();  
				
			}
			
			fw.close();
			fr.close();
			/**
			 * Error programado por si no se encuentra el archivo.
			 */
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Archivo no encontrado.",
					"Compruebe el nombre del archivo.",
					JOptionPane.ERROR_MESSAGE);
		}
		
		System.out.println("Ended.");
		
	}

}
